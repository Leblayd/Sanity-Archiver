﻿using System.IO;

namespace SanityArchiver.Application.Models.Abstract
{
    public abstract class Item
    {
        private readonly FileSystemInfo _info;
        public string Name => _info.Name;
        public string FullName => _info.FullName;

        protected Item(FileSystemInfo info)
        {
            _info = info;
        }

        public abstract long Size();
        public abstract void MoveTo(string fullName);
        public abstract void CopyTo(string fullName);
    }
}