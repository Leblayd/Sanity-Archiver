﻿using System;
using System.IO;
using System.IO.Compression;
using SanityArchiver.Application.Models.Abstract;

namespace SanityArchiver.Application.Models
{
    public class File : Item
    {
        private readonly FileInfo _info;

        public string ShortName => _info.Extension.Length == 0 ? _info.Name : _info.Name.Replace(_info.Extension, "");
        public string Extension => _info.Extension;
        public DateTime Created => _info.CreationTime;

        public File(string path) : this(new FileInfo(path))
        {
        }
        public File(FileInfo info) : base(info)
        {
            _info = info;
        }

        public override long Size()
        {
            return _info.Length;
        }

        public override void MoveTo(string path)
        {
            string newFileName = $@"{path}\{Name}";
            Directory.CreateDirectory(path);
            System.IO.File.Move(FullName, newFileName);
            // TODO probably necessary to dispose of "this" somehow, since the original shouldn't exist
        }

        public override void CopyTo(string path)
        {
            string newFileName = $@"{path}\{Name}";
            Directory.CreateDirectory(path);
            System.IO.File.Copy(FullName, newFileName);
        }

        public void Decompress()
        {
            string folderPath = $@"{_info.DirectoryName}\{ShortName}";
            Directory.CreateDirectory(folderPath);
            ZipFile.ExtractToDirectory(FullName, folderPath);
        }

        public override string ToString()
        {
            return ShortName + Extension;
        }
    }
}
