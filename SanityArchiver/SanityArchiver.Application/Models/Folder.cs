﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using SanityArchiver.Application.Models.Abstract;

namespace SanityArchiver.Application.Models
{
    public class Folder : Item
    {
        private readonly DirectoryInfo _info;

        public bool IsReady; // TODO

        public List<Folder> Folders
        {
            get
            {
                try
                {
                    List<DirectoryInfo> childrenInfo = new List<DirectoryInfo>();
                    if (_info.Exists) childrenInfo.AddRange(_info.EnumerateDirectories());

                    return childrenInfo
                        .Select(di => new Folder(di))
                        .ToList();
                }
                catch (UnauthorizedAccessException)
                {
                    return new List<Folder>();
                }
            }
        }

        public List<File> Files
        {
            get
            {
                List<FileInfo> filesInfo = new List<FileInfo>();
                try
                {
                    if (_info.Exists) filesInfo.AddRange(_info.EnumerateFiles());
                    return filesInfo
                        .Select(fi => new File(fi))
                        .ToList();
                }
                catch (UnauthorizedAccessException)
                {
                    return new List<File>();
                }
            }
        }

        public List<Item> Children
        {
            get
            {
                List<Item> children = new List<Item>();
                children.AddRange(Folders);
                children.AddRange(Files);
                return children;
            }
        }

        public Folder(DirectoryInfo info) : base(info)
        {
            _info = info;
        }

        public override long Size()
        {
            long filesSize = Files.Select(file => file.Size()).Sum();
            long foldersSize = Folders.Select(folder => folder.Size()).Sum();
            return filesSize + foldersSize;
        }

        public void Compress(string folderPath, string fileName)
        {
            string zipPath = $@"{folderPath}\{fileName}";
            ZipFile.CreateFromDirectory(FullName, zipPath);
        }

        public override void MoveTo(string fullName)
        {
            throw new NotImplementedException();
        }

        public override void CopyTo(string fullName)
        {
            throw new NotImplementedException();
        }
    }
}