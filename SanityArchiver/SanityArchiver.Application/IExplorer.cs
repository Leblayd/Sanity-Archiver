﻿using SanityArchiver.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SanityArchiver.Application.Models.Abstract;
using System.Collections.Specialized;

namespace SanityArchiver.Application
{
    public interface IExplorer
    {
        List<Item> Children();
        List<Folder> Folders();
        List<File> Files();
        void MoveTo(StringCollection items, string path);
        void CopyTo(StringCollection items, string path);
        void Compress(IEnumerable<Item> fileList, string zipName);
        void Decompress(IEnumerable<File> files);
        List<Folder> GetAllDrives();
    }
}
