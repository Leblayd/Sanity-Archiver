﻿using SanityArchiver.Application.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SanityArchiver.Application.Models.Abstract;
using File = SanityArchiver.Application.Models.File;
using System.Collections.Specialized;

namespace SanityArchiver.Application
{
    public class FileExplorer : IExplorer
    {
        private readonly string _tempPath = System.IO.Path.GetTempPath() + "SanityArchiver\\";
        public string Path { get; }
        private Folder Folder { get; }

        public FileExplorer(string path)
        {
            Path = path;
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            if (!directoryInfo.Exists) throw new DirectoryNotFoundException();

            Folder = new Folder(directoryInfo);
        }

        public List<Item> Children()
        {
            return Folder.Children;
        }

        public List<Folder> Folders()
        {
            return Folder.Folders;
        }

        public List<File> Files()
        {
            return Folder.Files;
        }

        private void MoveTo(Item item, string path)
        {
            item.MoveTo(path);
        }

        public void MoveTo(StringCollection items, string path)
        {
            foreach (string item in items)
            {
                // TODO handle moving folders, too
                MoveTo(new File(item), path);
            }
        }

        private void CopyTo(Item item, string path)
        {
            item.CopyTo(path);
        }

        private void CopyTo(IEnumerable<Item> items, string path)
        {
            foreach (Item item in items)
            {
                CopyTo(item, path);
            }
        }

        public void CopyTo(StringCollection items, string path)
        {
            foreach (string item in items)
            {
                // TODO copying moving folders, too
                CopyTo(new File(item), path);
            }
        }

        public void Compress(IEnumerable<Item> itemList, string zipName)
        {
            Compress(itemList, zipName, new FileInfo(itemList.First().FullName).DirectoryName);
        }

        public void Compress(IEnumerable<Item> itemList, string zipName, string folderPath)
        {
            DirectoryInfo tempFolderInfo = new DirectoryInfo(_tempPath + DateTime.Now.Ticks);

            CopyTo(itemList, tempFolderInfo.FullName);

            Folder tempFolder = new Folder(tempFolderInfo);
            string extension = ".zip";
            tempFolder.Compress(folderPath, zipName + extension);

            tempFolderInfo.Delete(true);
        }

        private static void Decompress(File file)
        {
            // TODO check if file is compressed
            file?.Decompress();
        }

        public void Decompress(IEnumerable<File> fileList)
        {
            foreach (File file in fileList)
            {
                Decompress(file);
            }
        }

        public List<Folder> GetAllDrives()
        {
            return DriveInfo.GetDrives().Where(drive => drive.IsReady).Select(drive => new Folder(drive.RootDirectory)).ToList();
        }
    }
}
