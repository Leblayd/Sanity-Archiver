﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using SanityArchiver.DesktopUI.Annotations;

namespace SanityArchiver.DesktopUI.ViewModels
{
    public class AddressBarVm : INotifyPropertyChanged
    {
        public string Text => "";
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}