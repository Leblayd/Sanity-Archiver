﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using SanityArchiver.Application.Models.Abstract;
using SanityArchiver.DesktopUI.Annotations;
using SanityArchiver.DesktopUI.Converters;

namespace SanityArchiver.DesktopUI.ViewModels
{
    public class StatusBarVm : INotifyPropertyChanged
    {
        public string Text { get; set; } = "placeholder";

        internal void FillStatusBar(IList<Item> selectedFiles)
        {
            string selected =
                selectedFiles.Count == 1
                    ? $"{selectedFiles[0]} is selected."
                    : $"{selectedFiles.Count} items selected.";

            long size = selectedFiles.Sum(item => item.Size());
            string totalSize = $"{DoubleToFileSizeConverter.LongToString(size)} in total.";

            Text = $"{selected} {totalSize}";
            OnPropertyChanged(nameof(Text)); // TODO why doesn't this work?
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}