﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace SanityArchiver.DesktopUI.Converters
{
    public class DoubleToFileSizeConverter : IValueConverter
    {
        private static readonly int _threshold = 800;
        private static readonly List<string> Names = new List<string> { "B", "KB", "MB", "GB", "TB", "PB" };
        
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return LongToString((long)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new InvalidOperationException("DoubleToFileSizeConverter can only be used OneWay.");
        }

        public static string LongToString(long number)
        {
            int i = 0;
            while (number > _threshold)
            {
                number /= 1000;
                i++;
            }
            return Math.Round((double)number, 2) + " " + Names[i];
        }
    }
}
