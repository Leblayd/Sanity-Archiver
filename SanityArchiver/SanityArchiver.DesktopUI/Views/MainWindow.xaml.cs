﻿using System;
using SanityArchiver.Application;
using SanityArchiver.DesktopUI.Converters;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SanityArchiver.Application.Models;
using SanityArchiver.Application.Models.Abstract;
using SanityArchiver.DesktopUI.Annotations;
using SanityArchiver.DesktopUI.ViewModels;
using File = SanityArchiver.Application.Models.File;
using System.Collections.Specialized;
using System.Threading.Tasks;

namespace SanityArchiver.DesktopUI.Views
{
    public partial class MainWindow
    {
        public string Path { get; private set; }
        private IExplorer Explorer { get; set; }
        public bool CutInsteadOfCopy { get; set; }
        public IList<Item> SelectedFiles { get; private set; }

        public StatusBarVm StatusBarVm { get; } = new StatusBarVm();
        public AddressBarVm AddressBarVm { get; } = new AddressBarVm();

        public MainWindow()
        {
            InitializeComponent();

            LoadFileList(@"C:\");
            FolderTreeView.ItemsSource = Explorer.GetAllDrives();
        }

        private void LoadFileList(string path)
        {
            Explorer = new FileExplorer(path);
            FileListView.ItemsSource = Explorer.Files();
            Path = path;
            AddressBar.Text = path;
        }

        private void FileListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedFiles = FileListView.SelectedItems.Cast<Item>().ToList();
            StatusBarVm.FillStatusBar(SelectedFiles);
            StatusBar.Text = StatusBarVm.Text; // temporary, should be done with event
        }

        private void FileListView_MouseDown(object sender, MouseButtonEventArgs e)
        {
            FileListView.UnselectAll();
        }
        
        private void TextBlock_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            LoadFileList(((Folder)((TextBlock)sender).DataContext).FullName);
        }

        private void AddressBar_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    try
                    {
                        LoadFileList(((TextBox)sender).Text);
                    }
                    catch (DirectoryNotFoundException)
                    {
                        goto case Key.Escape;
                    }
                    ((TextBox)sender).Text = Path;
                    goto case Key.Escape;
                case Key.Escape:
                    FileListView.Focus();
                    break;
            }
        }

        private void AddressBar_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            AddressBar.Text = Path;
        }

        private void ToClipboard(DragDropEffects effect)
        {
            DataObject data = new DataObject();
            StringCollection files = new StringCollection();
            files.AddRange(SelectedFiles.Select(item => item.FullName).ToArray());
            data.SetFileDropList(files);
            data.SetData("Preferred DropEffect", new MemoryStream(BitConverter.GetBytes((int)effect)));
            Clipboard.SetDataObject(data, true);
        }

        private void CutItem_Click(object sender, RoutedEventArgs e)
        {
            CutInsteadOfCopy = true;
            ToClipboard(DragDropEffects.Move);
        }

        private void CopyItem_Click(object sender, RoutedEventArgs e)
        {
            CutInsteadOfCopy = false;
            ToClipboard(DragDropEffects.Copy);
        }

        private void PasteItem_Click(object sender, RoutedEventArgs e)
        {
            if (CutInsteadOfCopy)
            {
                Explorer.MoveTo(Clipboard.GetFileDropList(), Path);
            }
            else
            {
                Explorer.CopyTo(Clipboard.GetFileDropList(), Path);
            }
            LoadFileList(Path);
        }

        private void Encrypt_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Decrypt_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Compress_Click(object sender, RoutedEventArgs e)
        {
            string fileName = DateTime.Now.ToFileTime().ToString(); // TODO
            Explorer.Compress(SelectedFiles, fileName);
            LoadFileList(Path);
        }

        private void Decompress_Click(object sender, RoutedEventArgs e)
        {
            Explorer.Decompress(SelectedFiles.ToList().ConvertAll(x => (File)x));
            LoadFileList(Path);
        }

        private void Open_Click(object sender, RoutedEventArgs e)
        {
            // TODO Open not just the first one
            System.Diagnostics.Process.Start(SelectedFiles.First().FullName);
        }
    }
}
